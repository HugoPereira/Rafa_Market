﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rafa_market.Startup))]
namespace Rafa_market
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
