﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Rafa_market.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rafa_market.Helpers
{
    public class CombosHelper:IDisposable
    {
        private static Rafa_marketContext db = new Rafa_marketContext();

        private static ApplicationDbContext da = new ApplicationDbContext();


        public static List<DocumentType> GetDocumentTypes()
        {
            var DocumentTypes = db.DocumentTypes.ToList();
            DocumentTypes.Add(new DocumentType
            {
                DocumentTypeID = 0,
                Description = "[Selecione um Documento]"
            });

            return DocumentTypes.OrderBy(d => d.Description).ToList();
        }


        public static List<Customer> GetCustomersName()
        {
            var Customers = db.Customers.ToList();
            
            Customers.Add(new Customer
            {
                CustomerID = 0,
                CustomerFirstName = "[Selecione um Cliente]"
            });

            return Customers.OrderBy(c => c.Name).ToList();
        }


        public static List<Product> GetProducts()
        {
            var Products = db.Products.ToList();

            Products.Add(new Product
            {
                ProductID = 0,
                Description = "[Selecione um Produto]"
            });

            return Products.OrderBy(c => c.Description).ToList();
        }

        public static List<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(da));

            var list = roleManager.Roles.ToList();

            list.Add(new IdentityRole
            {
                Id = "",
                Name = "[Selecione uma permissão]"
            });
            
            return list.OrderBy(r => r.Name).ToList();
        }

        //para fechar o datacontext
        public void Dispose()
        {
            db.Dispose();
        }
    }
}