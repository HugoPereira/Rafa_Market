namespace Rafa_market.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Rafa_market.Models.Rafa_marketContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;

            //para permitir que dados se percam na migracao
            AutomaticMigrationDataLossAllowed = true;

            //metemos o caminho do Context
            ContextKey = "Rafa_market.Models.Rafa_marketContext";
        }

        protected override void Seed(Rafa_market.Models.Rafa_marketContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
