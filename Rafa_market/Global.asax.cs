﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Rafa_market.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Rafa_market
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //inicializa a bd e vai buscar as mudanças antes de arrancar
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Models.Rafa_marketContext, Migrations.Configuration>());

            //context da autenticacaco
            ApplicationDbContext db = new ApplicationDbContext();

            //criar os papeis para os users
            CreateRoles(db);

            //para criar o administrador, que gere tudo
            CreateSuperUser(db);

            AddpermitionsToSuperUser(db);

            db.Dispose();//fechar basedados

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void AddpermitionsToSuperUser(ApplicationDbContext db)
        {
            //ligacao aos users
            var usermanager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            //ligacao aos roles
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = usermanager.FindByName("hugop@gmail.com");

            if (!usermanager.IsInRole(user.Id, "View"))
            {
                usermanager.AddToRole(user.Id, "View");
            }

            if (!usermanager.IsInRole(user.Id, "Create"))
            {
                usermanager.AddToRole(user.Id, "Create");
            }

            if (!usermanager.IsInRole(user.Id, "Edit"))
            {
                usermanager.AddToRole(user.Id, "Edit");
            }

            if (!usermanager.IsInRole(user.Id, "Delete"))
            {
                usermanager.AddToRole(user.Id, "Delete");
            }
        }

        private void CreateSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("hugop@gmail.com");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "hugop@gmail.com",
                    Email = "hugop@gmail.com"
                };

                //password para o user
                userManager.Create(user,"hugo123.");
            }
        }

        private void CreateRoles(ApplicationDbContext db)
        {
            //                obj tipo role              guardado num rolestore
            var rolemanager = new RoleManager<IdentityRole> (new RoleStore<IdentityRole>(db));

            //para apagar um role
            //var role = rolemanager.FindByName("Creat");
            //rolemanager.Delete(role);

            //criamos regras para o crud, se nao existirem
            if (!rolemanager.RoleExists("View"))
            {
                rolemanager.Create(new IdentityRole("View"));
            }

            if (!rolemanager.RoleExists("Edit"))
            {
                rolemanager.Create(new IdentityRole("Edit"));
            }

            if (!rolemanager.RoleExists("Create"))
            {
                rolemanager.Create(new IdentityRole("Create"));
            }

            if (!rolemanager.RoleExists("Delete"))
            {
                rolemanager.Create(new IdentityRole("Delete"));
            }
        }
    }
}
