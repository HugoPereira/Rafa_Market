﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }

        [Display(Name = "Primeiro Nome")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Cliente")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string CustomerFirstName { get; set; }


        [Display(Name = "Apelido")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Cliente")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string CustomerLastName { get; set; }

        [Display(Name ="Nome")]
        [NotMapped]//nao fica na tabela apenas para uso do mvc
        public string Name { get { return $"{CustomerFirstName} {CustomerLastName}"; } }


        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Cliente")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} digitos", MinimumLength = 9)]
        public string CustomerPhone { get; set; }


        [Display(Name = "Morada")]
        [Required(ErrorMessage = "Tem que inserir uma {0} para o Cliente")]
        [StringLength(100, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} digitos", MinimumLength = 3)]
        public string CustomeAdress { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string CustomeEmail { get; set; }


        [Display(Name = "Nº Documento")]
        [Required(ErrorMessage = "Tem que inserir uma {0} para o Cliente")]
        [StringLength(20, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} digitos", MinimumLength = 4)]
        public string Document { get; set; }

        [Display(Name ="Documento")]
        [Required(ErrorMessage ="O {0} é obrigatório")]
        [Range(1, double.MaxValue, ErrorMessage ="Tem que selecionar um {0}")]//minimo selecionavel é 1
        public int DocumentTypeID { get; set; }

        //ligacao aos documentos
        public virtual DocumentType DocumentType { get; set; }

        //ligacao as encomendas
        public virtual ICollection<Order> Orders { get; set; }
    }


}