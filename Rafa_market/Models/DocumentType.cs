﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class DocumentType
    {
        [Key]
        [Display(Name ="Tipo Documento")]//o nome que eu quero que apareca
        public int DocumentTypeID { get; set; }

        [Display(Name = "Documento")]//o nome que eu quero que apareca
        public string Description { get; set; }


        //tras todos os empregados com o tipo de documento(faz a ligacao ao employee) ligacao de 1(dos employees), para muitos(Icollection)
        public virtual ICollection<Employee> Employees { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}