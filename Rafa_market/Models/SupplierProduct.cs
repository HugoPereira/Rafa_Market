﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class SupplierProduct
    {
        [Key]
        public int SupplierProductID { get; set; }

        public int SupplierID { get; set; }

        public int ProductID { get; set; }


        //faz a ligacao de um para mts, passando todos a ser de muitos para muitos
        public virtual Supplier Supplier { get; set; }

        public virtual Product Product { get; set; }
    }
}