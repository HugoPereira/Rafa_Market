﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    //[Table("Empregados")] cria a tabela com este nome
    public class Employee
    {
        [Key]
        public int EmployeeID { get; set; }

        //data anotations sao as anotacoes que quero que aparecam la
        [StringLength(30, ErrorMessage = "O {0} do produto deverá ter entre {2} e {1}", MinimumLength = 3)]
        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Tem que inserir o {0}")]//msg de erro
        public string FirstName { get; set; }

        [StringLength(30, ErrorMessage = "O {0} do produto deverá ter entre {2} e {1}", MinimumLength = 3)]
        [Display(Name = "Apelido")]
        [Required(ErrorMessage = "Tem que inserir o {0}")]//msg de erro
        public string LastName { get; set; }

        [Display(Name = "Salário")]
        [Required(ErrorMessage = "Tem que inserir um valor para o {0}")]//msg de erro
        public decimal Salary { get; set; }


        //[Column("Nome")] muda o nome tambem na base de dados
        [Display(Name = "Percentagem Bonus")]
        [Range(0, 20, ErrorMessage ="O valor da {0} deverá ser entre {1} e {2}")]//damos o valor entre que queremos
        public float BonusPercent { get; set; }


        [DataType(DataType.Date)]
        [Display(Name = "Data Nascimento")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]//msg de erro
        public DateTime DateOfBirth { get; set; }


        [DataType(DataType.Date)]
        [Display(Name = "Data de inicio")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]//msg de erro
        public DateTime StartTime { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        public string Url { get; set; }



        //[ForeignKey("DocumentType")] pouco usado / em vez da prop virtual podemos dizer que é uma chave estrangeira
        [Display(Name = "Tipo de Documento")]
        [Required(ErrorMessage = "Tem que inserir um {0}")]//msg de erro
        [Range(1, double.MaxValue, ErrorMessage = "Tem que selecionar um {0}")]//minimo selecionavel é 1
        //campo da outra tabela
        public int DocumentTypeID { get; set; }


        //vai retornar a idade
        [Display(Name ="Idade")]
        [NotMapped]//nao é incluido na base de dados, apenas info para o mvc
                   //public int Age { get { return DateTime.Now.Year - DateOfBirth.Year; } }

        public int Age
        {
            get
            {
                var myAge = DateTime.Now.Year - DateOfBirth.Year;

                if (DateOfBirth > DateTime.Now.AddYears(- myAge))
                {
                    myAge--;
                }
                return myAge;
            }
        }

        //o que faz a ligacao ao outra tabela, daai ser virtual
        public virtual DocumentType DocumentType { get; set; }
    }
}