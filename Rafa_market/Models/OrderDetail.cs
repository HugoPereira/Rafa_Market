﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class OrderDetail
    {
        [Key]
        public int OrderDetailID { get; set; }

        public int OrderID { get; set; }

        public int ProductID { get; set; }

        [Display(Name ="Descrição do Produto")]
        [Required(ErrorMessage = "Deve inserir uma {0}")]
        [StringLength(30, ErrorMessage = "A {0} do produto deverá ter entre {2} e {1}", MinimumLength = 3)]
        public string OrderDescription { get; set; }

        [Display(Name = "Preço")]
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]//2 casas decimais
        public decimal Price { get; set; }

        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]//2 casas decimais
        public float Quantity { get; set; }
        
        public virtual Order Order { get; set; }

        public virtual Product Products { get; set; }
    }
}