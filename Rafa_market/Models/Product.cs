﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace Rafa_market.Models
{
   
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Display(Name ="Descrição")]
        [StringLength(30, ErrorMessage ="A {0} do produto deverá ter entre {2} e {1}", MinimumLength =3)]
        [Required(ErrorMessage ="Deve inserir uma {0}")]
        public string Description { get; set; }

        [Display(Name ="Preço")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString ="{0:C2}", ApplyFormatInEditMode =false)]//2 casas decimais
        [Required(ErrorMessage ="Deve inserir um {0}")]
        public decimal Price { get; set; }

        [Display(Name = "Ultima Compra")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime LastBuy { get; set; }


        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]//2 casas decimais
        public float Stock { get; set; }

        //images
        public string Image { get; set; }

        [NotMapped]
        public HttpPostedFileBase ProductImageURL { get; set; }


        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        //muitos para muitos temos de usar uma tabela intermedia(supplierProduct)
        public virtual ICollection<SupplierProduct> SupplierProduct { get; set; }

        //ligacao aos detalhes da encomenda
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }


        

    }
}