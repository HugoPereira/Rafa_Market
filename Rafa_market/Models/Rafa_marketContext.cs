﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class Rafa_marketContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public Rafa_marketContext() : base("name=Rafa_marketContext")
        {
        }

        //para nao permitir que se apaguem objectos com relacoes na bd
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<Rafa_market.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.DocumentType> DocumentTypes { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.Employee> Employees { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.Supplier> Suppliers { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.Order> Orders { get; set; }

        public System.Data.Entity.DbSet<Rafa_market.Models.OrderDetail> OrderDetails { get; set; }
    }
}
