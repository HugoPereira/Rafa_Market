﻿using System.ComponentModel.DataAnnotations;

namespace Rafa_market.Models
{
    public class ProductOrder:Product
    {
        [Display(Name = "Quantidade")]
        [Required(ErrorMessage = "Deve inserir um {0}")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = false)]//2 casas decimais
        public float Quantity { get; set; }

        [Display(Name = "Valor a Pagar")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]//2 casas decimais
        public decimal Value { get { return Price * (decimal)Quantity; } }
    }
}