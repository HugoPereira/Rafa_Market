﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rafa_market.Models
{
    public class Supplier
    {
        [Key]
        public int SupplierID { get; set; }

        [Display(Name ="Nome")]
        [Required(ErrorMessage ="Tem que inserir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage ="O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength =3)]
        public string SupplierName { get; set; }

        [Display(Name ="Nome do Contacto")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string ContactFirstName { get; set; }

        [Display(Name = "Apelido do Contacto")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o Fornecedor")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string ContactLastName { get; set; }

        [Display(Name = "Telefone")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Tem que inserir um {0}")]
        [StringLength(30, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} digitos", MinimumLength = 9)]
        public string Phone { get; set; }


        [Display(Name = "Morada")]
        [Required(ErrorMessage = "Tem que inserir uma {0}")]
        [StringLength(100, ErrorMessage = "O Campo {0} deve conter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string Adress { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        //muitos para muitos temos de usar uma tabela intermedia(supplierProduct)
        public virtual ICollection<SupplierProduct> SupplierProduct { get; set; }
    }
}