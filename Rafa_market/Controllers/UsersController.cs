﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Rafa_market.Helpers;
using Rafa_market.Models;
using Rafa_market.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Rafa_market.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Users
        public ActionResult Index()
        {
            var usermanager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = usermanager.Users.ToList();

            var usersView = new List<UserView>();

            foreach (var user in users)
            {
                var userView = new UserView
                {
                    Email = user.Email,
                    Name = user.UserName,
                    UserID = user.Id
                };

                usersView.Add(userView);
            }

            return View(usersView);
        }

        //GET Roles
        public ActionResult Roles(string UserID)
        {
            if (string.IsNullOrEmpty(UserID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var usermanager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();

            var users = usermanager.Users.ToList();

            var user = users.Find(u => u.Id == UserID);

            if (user == null)
            {
                return HttpNotFound();
            }

            var rolesview = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                var role = roles.Find(r => r.Id == item.RoleId);
                var roleview = new RoleView
                {
                    RoleID = role.Id,
                    RoleName = role.Name

                };

                rolesview.Add(roleview);
            }

            var userView = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                UserID = user.Id,
                Roles = rolesview
                
            };

            return View(userView);
        }

        //GET Roles
        
        public ActionResult AddRole(string UserID)
        {
            if (string.IsNullOrEmpty(UserID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var usermanager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = usermanager.Users.ToList();

            var user = users.Find(u => u.Id == UserID);

            if (user == null)
            {
                return HttpNotFound();
            }

            var userview = new UserView
            {
                Email = user.UserName,
                Name = user.UserName,
                UserID = user.Id

            };

           
            ViewBag.RoleID = new SelectList(CombosHelper.GetRoles(), "Id", "Name");
            
            return View(userview);
        }

        //POST Roles
        [HttpPost]
        public ActionResult AddRole(string UserID, FormCollection form)
        {
            var RoleID = Request["RoleID"];

            var usermanager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var users = usermanager.Users.ToList();

            var user = users.Find(u => u.Id == UserID);

           
            var userview = new UserView
            {
                Email = user.UserName,
                Name = user.UserName,
                UserID = user.Id

            };

            if (string.IsNullOrEmpty(RoleID))
            {
                ViewBag.Error = "Tem de selecionar uma permissão";

                ViewBag.RoleID = new SelectList(CombosHelper.GetRoles(), "Id", "Name");

                return View(userview);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var roles = roleManager.Roles.ToList();
            var role = roles.Find(r => r.Id == RoleID);

            //se nao tiver esta permissao, vai atribuir
            if (!usermanager.IsInRole(UserID, role.Name))
            {
                usermanager.AddToRole(UserID, role.Name);
            }

            //crio a lista das roles
            var rolesView = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == item.RoleId);

                var roleView = new RoleView
                {
                    RoleName = role.Name,
                    RoleID = role.Id
                };

                //adiciono a lista
                rolesView.Add(roleView);
            }

            //adiciono ao user
            userview = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                Roles = rolesView,
                UserID = user.Id
            };

            //retorno as roles
            return View("Roles",userview);
        }


        public ActionResult Delete(string userID, string roleID)
        {
            if (string.IsNullOrEmpty(userID) || string.IsNullOrEmpty(roleID))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.Users.ToList().Find(u => u.Id == userID);

            var role = roleManager.Roles.ToList().Find(r => r.Id == roleID);

            //apagar o user deste role
            if (userManager.IsInRole(user.Id, role.Name))
            {
                userManager.RemoveFromRole(user.Id, role.Name);
            }

            //preparar a view

            var users = userManager.Users.ToList();

            var roles = roleManager.Roles.ToList();

            var rolesview = new List<RoleView>();

            foreach (var item in user.Roles)
            {
                role = roles.Find(r => r.Id == roleID);

                var roleView = new RoleView
                {
                    RoleName = role.Name,
                    RoleID = role.Id
                };

                rolesview.Add(roleView);

            }

            //adiciono ao user
            var userview = new UserView
            {
                Email = user.Email,
                Name = user.UserName,
                Roles = rolesview,
                UserID = user.Id
            };

            return View("Roles",userview);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}