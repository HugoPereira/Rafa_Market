﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rafa_market.Helpers;
using Rafa_market.Models;

namespace Rafa_market.Controllers
{
    //so entra com login, tb pode ser colocada em cada action
    [Authorize]

    //[Authorize(Users ="12494@formandos.cinel.pt")]//digo quais os users autorizados, separado por virgulas
    public class ProductsController : Controller
    {
        private Rafa_marketContext db = new Rafa_marketContext();

        //[AllowAnonymous] //permite anonimos verem
        // GET: Products
        [Authorize(Roles ="View")]//so quem tiver esta permissao e que epode
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        [Authorize(Roles = "View")]//so quem tiver esta permissao e que epode
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Create")]//so quem tiver esta permissao e que epode
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,Description,Price,LastBuy,Stock,ProductImageURL,Image,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();

                if (product.ProductImageURL != null)
                {
                    var folder = "~/Content/Images";

                    var file = string.Format("{0}.png", product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageURL, file, folder);

                    if (response)
                    {
                        var pic = string.Format("{0}/{1}", folder, file);

                        product.Image = pic;

                        db.Entry(product).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Edit")]//so quem tiver esta permissao e que epode
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,Description,Price,LastBuy,Stock,Image,ProductImageURL,Remarks")] Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.ProductImageURL != null)
                {
                    var pic = string.Empty;

                    var folder = "~/Content/Images";

                    var file = string.Format("{0}.png", product.ProductID);

                    var response = FilesHelper.UploadImage(product.ProductImageURL, file, folder);

                    if (response)
                    {
                        pic = string.Format("{0}/{1}", folder, file);

                        product.Image = pic;
                    }
                }

                db.Entry(product).State = EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Delete")]//so quem tiver esta permissao e que epode
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
