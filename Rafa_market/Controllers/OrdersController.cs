﻿using Rafa_market.Helpers;
using Rafa_market.Models;
using Rafa_market.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rafa_market.Controllers
{
    public class OrdersController : Controller
    {
        private Rafa_marketContext db = new Rafa_marketContext();
        // GET: Orders
        public ActionResult NewOrder()
        {
            var orderView = new OrderView();
            orderView.Customer = new Customer();
            orderView.Products = new List<ProductOrder>();

            //variavel de sessao para passar para outra action(mas de vista diferentes)
            Session["orderView"] = orderView;

            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

            return View(orderView);
        }


        [HttpPost]
        public ActionResult NewOrder(OrderView orderView)
        {
            orderView = Session["orderView"] as OrderView;

            var CustomerID = int.Parse(Request["CustomerID"]);

            if (CustomerID== 0)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

                ViewBag.Error = "Deve selecionar um cliente";

                return View(orderView);
            }

            var customer = db.Customers.Find(CustomerID);

            if (customer == null)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

                ViewBag.Error = "O cliente não existe";

                return View(orderView);
            }

            if (orderView.Products.Count == 0)
            {
                ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

                ViewBag.Error = "Deve escolher os produtos a encomendar";

                return View(orderView);
            }

            //comeca a gravar

            int orderId = 0;

            //usado em registos relacionados em multiplas tabelas
            //guarda em memoria e so gravo qd estiver tudo bem
            using (var Transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var order = new Order
                    {
                        CustomerID = CustomerID,
                        OrderDate = DateTime.Now,
                        OrderStatus = OrderStatus.Created
                    };

                    db.Orders.Add(order);
                    db.SaveChanges();

                    orderId = order.OrderID;

                    foreach (var item in orderView.Products)
                    {
                        var orderDetail = new OrderDetail
                        {
                            OrderDescription = item.Description,
                            Price = item.Price,
                            Quantity = item.Quantity,
                            OrderID = orderId,
                            ProductID = item.ProductID
                        };

                        db.OrderDetails.Add(orderDetail);

                        db.SaveChanges();
                    }

                    //gravo todo processo que esta em memoria, pois se passou ate aqui esta ok.
                    Transaction.Commit();
                }
                catch (Exception e)
                {
                    //se alguma coisa correu mal, volta a tras
                    Transaction.Rollback();

                    ViewBag.Error = $"Erro {e.Message}";
                    ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

                    return View(orderView);
                }
            }
            
            ViewBag.Message = $"A encomenda: {orderId} for efectuada com sucesso";

            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

            //prepara para criar nova encomenda
            orderView = new OrderView();
            orderView.Customer = new Customer();
            orderView.Products = new List<ProductOrder>();

            Session["orderView"] = orderView;

            return View(orderView);
            //return RedirectToAction("NewOrder");
        }



        public ActionResult AddProduct()
        {
            ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");

            return View();
        }

        [HttpPost]
        public ActionResult AddProduct(ProductOrder productOrder)
        {
            var orderView = Session["orderView"] as OrderView;
            
            var ProductId = int.Parse(Request["ProductID"]);

            //caso nao haja produto escolhido
            if (ProductId == 0)
            {
                ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");

                ViewBag.Error = "Deve selecionar um produto";

                return View(productOrder); 
            }

            //verifica se produto existe

            var Product = db.Products.Find(ProductId);

            if (Product == null)
            {
                ViewBag.ProductID = new SelectList(CombosHelper.GetProducts(), "ProductID", "Description");

                ViewBag.Error = "Produto não existe";

                return View(productOrder);
            }

            //verifica se existe
            productOrder = orderView.Products.Find(p => p.ProductID == ProductId);

            //caso nao exista, insere
            if (productOrder == null)
            {
                productOrder = new ProductOrder
                {
                    Description = Product.Description,
                    Price = Product.Price,
                    ProductID = Product.ProductID,
                    Quantity = float.Parse(Request["Quantity"])
                };

                //adicionar a lista
                orderView.Products.Add(productOrder);
            }
            else
            {
                productOrder.Quantity += float.Parse(Request["Quantity"]);
            }

            
            ViewBag.CustomerID = new SelectList(CombosHelper.GetCustomersName(), "CustomerID", "Name");

            return View("NewOrder", orderView);
        }


        //fechar o datacontext
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}